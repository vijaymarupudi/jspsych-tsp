# Instructions

You will see some points displayed on the screen each trial. Your task is to
connect the points so that the path you create is the **SHORTEST POSSIBLE PATH**
that connects all the points.

You will choose the point you want to connect to using your mouse. Click at the
point you want to connect to, and you will see a line connecting the point to
your path. After you are done connecting the points, you will be shown the entire
path you have drawn for a moment before proceeding to the next trial.

To start the trial, you will need to click your starting point. Keep in mind that the path you indicate will connect back to the first point, and your task is to make this complete path **as short as possible**.


You may take short breaks between trials, but otherwise **please give each task
your undivided attention**. The study should not take more than an hour from
start to finish.

Click next to continue to practice trials.
