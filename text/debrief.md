The purpose of this study is to investigate how young adults solve spatial math
problems.

Previous studies have identified that there are some cognitive abilities that
are available early in development and that form the basis for understanding
mathematical concepts. For example, people have **magnitude representations**
analogous to a mental number line, and use them to understand the size of
numbers (Halberda, Mazzocco, & Feigenson, 2008; Moyer & Landauer, 1967). Most
prior research has focused on number concepts. Comparatively little is known
about the cognitive abilities underlying understanding of spatial mathematical
concepts.

The current study has the goal of finding evidence that the ability to
efficiently **cluster** points in space is foundational for understanding
concepts in graph theory, an abstract branch of mathematics. A prior experiment
established that clustering is a stable human ability. The current experiment
investigated how people solve an important problem in graph theory, the
traveling salesperson problem (TSP). Prior research has found that people solve
TSP problems efficiently. In particular, as the number of points (i.e., the
size) of a problem increases, their solution time increases, but only
“linearly” (Graham, Joshi, & Pizlo, 2000; MacGregor & Ormerod 1996; van Rooij,
Stege, & Schactman, 2003). This is surprising because computers have a hard
time solving TSP problems! In fact, as problem size increases, their solution
time increases “exponentially”. Or, more colorfully, they “blow up”! The novel
contribution of this experiment is to investigate whether people’s solutions
follow the cluster structure of problems.

You solved a set of TSP problems twice. Each problem was an arrangement of
10-40 points in a rectangle. For half, the points were “non-randomly”
scattered, i.e., contained naturally occurring clusters. For the other half,
they were “randomly” scattered. Solving a problem required connecting the
points in a tour of minimal length. The second time you solved the problems,
the points of half of them were flipped horizontally and vertically.

We will analyze your data to address four research questions:

* Is human TSP performance **stable**? Do people produce the same tours when
  they solve the same problem on multiple occasions?

* Is human TSP performance efficient and optimal? As the number of points
  increases, do solution times increase, but only “linearly”? Are the tours
  that people produce close to the minimal length?

* Is human TSP performance sensitive to the cluster structure of problems? Do
  people produce more efficient and more optimal solutions if a problem
  contains naturally occurring clusters than if it does not?

* Is the stability of human TSP performance affected by spatial transformation?
  Are people less stable when the points of a problem are flipped the second
  time they view it?

This experiment is also investigating a question of individual differences. We
will analyze whether your TSP performance is associated with your ACT-Math
score. 

If you have any further questions, please ask the experimenter at
<marupudi@umn.edu>.
