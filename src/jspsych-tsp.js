import "./style.css";

function clear(ctx) {
  ctx.clearRect(0, 0, 800, 500);
}

function isSamePoint(a, b) {
  if (a.x === b.x && a.y === b.y) return true;
}

function getEventXY(canvas, event) {
  return [event.pageX - canvas.offsetLeft, event.pageY - canvas.offsetTop];
}

function drawPoints(ctx, points, color) {
  return points.map(({ x, y }) => {
    ctx.beginPath();
    ctx.arc(x, y, 5, 0, Math.PI * 2);
    ctx.save();
    ctx.fillStyle = color;
    ctx.fill();
    ctx.restore();
    // larger hitbox for easier clicking
    const path = new Path2D();
    path.arc(x, y, 8, 0, Math.PI * 2); // 10 is the hitbox
    return { x, y, path };
  });
}

function connectPoints(ctx, points, color, connect) {
  ctx.save();
  ctx.strokeStyle = color;
  ctx.beginPath();
  for (const { x, y } of points) {
    ctx.lineTo(x, y);
  }

  if (connect) {
    ctx.closePath()
  }

  ctx.stroke();
  ctx.restore();
}

function handle(canvas, points, callback) {
  const ctx = canvas.getContext("2d");
  const tspData = [];

  let mappedPoints = null;

  function redraw(final = false) {
    clear(ctx);
    mappedPoints = drawPoints(ctx, points, "#000000");
    drawPoints(ctx, tspData, "#0000FF");
    connectPoints(ctx, tspData, "#0000FF", final);
  }

  function posteventcheck() {
    if (tspData.length === points.length) {
      canvas.removeEventListener('click', clicklistener)
      redraw(true); // initial draw
      setTimeout(() => {
        callback(tspData)
      }, 1000)
    } else {
      redraw()
    }
  }

  redraw(); // initial draw

  const clicklistener = e => {
    const [x, y] = getEventXY(canvas, e);

    // if didn't click a point, do nothing
    const matchedPointList = mappedPoints.filter(item => {
      return ctx.isPointInPath(item.path, x, y);
    });
    if (matchedPointList.length === 0) {
      return;
    }
    const matchedPoint = matchedPointList[0];

    // if clicked already picked tsp point, do nothing
    const alreadyClickedPointList = tspData.filter(item =>
      isSamePoint(matchedPoint, item)
    );
    if (alreadyClickedPointList.length !== 0) return;

    // push point to data, do a posteventcheck
    tspData.push({ ...matchedPoint, timestamp: e.timeStamp });
    posteventcheck()

  }

  canvas.addEventListener("click", clicklistener);
}

function plugin(display_element, trial) {
  const canvas = document.createElement("canvas");
  canvas.setAttribute("width", 800);
  canvas.setAttribute("height", 500);
  canvas.classList.add("jspsych-traveling-salesman");
  display_element.innerHTML = "";
  display_element.appendChild(canvas);

  const stimulus = trial.stimulus;

  const startTimestamp = performance.now()
  handle(canvas, stimulus.points, tspData => {
    const stopTimestamp = tspData[tspData.length - 1].timestamp;
    const trialData = {
      startDateTime: new Date().valueOf(),
      startTimestamp,
      stopTimestamp,
      tsp: tspData,
      stimulus: stimulus
    }
    jsPsych.finishTrial(trialData)
  });
}

export default function jsPsychTravelingSalesman(jsPsych) {
  jsPsych.plugins["tsp"] = {
    info: { name: "tsp", parameters: {} },
    trial: plugin
  };
}
