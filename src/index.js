// # CSS
// jsPsych
import "../jspsych-6.1.0/css/jspsych.css";
// Others
import "./style.css";

// JS
// jsPsych
import "../jspsych-6.1.0/jspsych";
import "../jspsych-6.1.0/plugins/jspsych-instructions";
import "../jspsych-6.1.0/plugins/jspsych-survey-html-form";
import "../jspsych-6.1.0/plugins/jspsych-html-keyboard-response";
import "../jspsych-6.1.0/plugins/jspsych-html-button-response";
import "../jspsych-6.1.0/plugins/jspsych-survey-text";
import "../jspsych-6.1.0/plugins/jspsych-fullscreen";

// Others
import jsPsychFactorialEstimationPlugin from "./jspsych-factorial-estimation";
import jsPsychTSPPlugin from "./jspsych-tsp"
import addFactorial from "./addon-factorial-estimation";
import { filterInt } from "./jspsych-utils";

import { CONSENT_FORM, DEMOGRAPHICS, INSTRUCTIONS, DEBRIEFING, END_HTML } from "./CONSTANTS"

import axios from "axios";


 async function serverlog(message) {
   const participantNumber = state.participantNumber && state.participantNumber.toString() || "UNKNOWN";
   return axios.post("/api/tsp/log", { participantNumber, message })
 }


const state = {};

function getNavigatorData() {
  const _navigator = {};
  for (const i in navigator) {
    _navigator[i] = navigator[i];
  }
  return _navigator;
}

function normalText(text) {
  return "<div class='normal-text'>" + text + "</div>";
}

async function onExperimentEnd() {
  const experimentData = jsPsych.data.get().values();
  const interactionData = jsPsych.data.getInteractionData().values();
  const navigatorData = getNavigatorData();
  try {
    jsPsych.getDisplayElement().innerHTML = `<p>Uploading data, please wait...</p>`;
    await axios.post("/api/tsp/save", {
      participantNumber: state.participantNumber,
      data: {
        experimentData,
        interactionData,
        navigatorData
      }
    });
    jsPsych.getDisplayElement().innerHTML = normalText(END_HTML);
  } catch (err) {
    console.error(err);
    jsPsych.getDisplayElement().innerHTML = `<p><b>An error has occurred. Do not worry, you will still be paid. Please inform the experimenter of this error message.</b></p>

          <p>${err.name}: ${err.message}</p>`;
  }
}

async function main() {
  // Initiate custom jsPsych plugins
  jsPsychTSPPlugin(jsPsych);
  jsPsychFactorialEstimationPlugin(jsPsych);

  const stimuliStore = {};

  const practiceUUIDs = jsPsych.randomization.shuffle(
    (await axios.get("stimuli/practice_uuids.json")).data
  );
  const setBlockUUIDConfiguration = (await axios.get(
    "stimuli/set_block_unique_uuids.json"
  )).data;
  const setBlockUUIDs = [];
  for (const set of Object.keys(setBlockUUIDConfiguration)) {
    for (const block of Object.keys(setBlockUUIDConfiguration[set])) {
      setBlockUUIDs.push(...setBlockUUIDConfiguration[set][block]);
    }
  }

  // Consolidate and remove duplicates
  const allUUIDs = [...practiceUUIDs, ...setBlockUUIDs].filter(
    (value, index, self) => {
      return self.indexOf(value) === index;
    }
  );

  const promises = allUUIDs.map(async uuid =>
    (await axios.get("stimuli/stimuli_json/" + uuid + ".json")).data
  );

  const allStimuli = await Promise.all(promises);
  allStimuli.forEach(stim => {
    stimuliStore[stim["unique_uuid"]] = stim;
  });

  const SET = 2;

  const timeline = [];

  const addBlockToTimeline = blockNumber => {
    const blockUUIDs = setBlockUUIDConfiguration[SET][blockNumber];
    const randomBlockUUIDs = jsPsych.randomization.shuffle(blockUUIDs);
    for (const stimulusUUID of randomBlockUUIDs) {
      timeline.push({
        type: "html-button-response",
        stimulus: '<p>Click continue to start trial.</p>',
        choices: ["Continue"],
        data: {
          questionId: "pre-trial-button"
        }
      })

      timeline.push({
        type: "tsp",
        stimulus: stimuliStore[stimulusUUID],
        data: {
          block: blockNumber,
          set: SET
        }
      });
    }
  };

  const addBreak = () => {
    timeline.push({
      type: "html-keyboard-response",
      stimulus: `<p>Take a short break if you'd like.</p>
    <p>Press [spacebar] to continue</p>`,
      choices: [32],
      data: { questionId: "break" }
    });
  };

  const addIntroAndPracticeTrials = () => {
    // Mouse question
    timeline.push({
      type: "html-button-response",
      stimulus: `<p><b>We strongly recommend that you use a mouse for this study. If possible, please connect a mouse and use it.</b></p>
      <p>Are you going to use a mouse for this study?</p>`,
      choices: ["Yes", "No"],
      data: { questionId: "mouseQuestion" }
    });

    // Fullscreen
    timeline.push({
      type: "fullscreen",
      fullscreen_mode: true,
      data: { questionId: "fullscreenRequest" }
    });

    const subtimeline = []; // for looping during practice

    // Introduction
    subtimeline.push({
      type: "instructions",
      pages: [normalText(INSTRUCTIONS)],
      show_clickable_nav: true,
      data: { questionId: "tspInstructions" }
    });

    // Practice trials
    subtimeline.push(
      ...practiceUUIDs.map((uuid, idx) => ({
        type: "tsp",
        stimulus: stimuliStore[uuid],
        data: { questionId: "tspPracticeTrial", practiceTrialIdx: idx }
      }))
    );

    // Start over or Continue?
    subtimeline.push({
      type: "html-button-response",
      stimulus:
        "<p>Did you understand the task at hand? Click <i>continue</i> to start the experiment, or <i>start over</i> to practice again.</p>",
      choices: ["Start over", "Continue"],
      data: { questionId: "practiceStartOver" }
    });

    // Add to full timeline
    timeline.push({
      timeline: subtimeline,
      loop_function: function(data) {
        if (filterInt(data.last().values()[0].button_pressed) === 0) {
          return true;
        }
        return false;
      }
    });
  };

  const addNoticeHTML = (html, questionId) => {
    timeline.push({
      type: "html-button-response",
      stimulus: html,
      choices: ["Continue"],
      ...(questionId ? { data: { questionId: questionId } } : {})
    });
  };

  const addConsent = () => {
    // Consent
    timeline.push({
      timeline: [
        {
          type: "survey-html-form",
          html: normalText(CONSENT_FORM)
        }
      ],
      // Make sure participant number is an integer, and then sets it as a global
      // variable for factorial to use.
      loop_function: function() {
        const data = jsPsych.data
          .get()
          .last()
          .values()[0];
        const consentResponses = JSON.parse(data.responses);
        const participantNumber = filterInt(consentResponses.participantNumber);

        // if not a number, loop back
        if (isNaN(participantNumber)) {
          return true; // show consent form again
        } else {
          state.participantNumber = participantNumber;
        }

        axios.post(`/api/tsp/consent`, {
          participantNumber: state.participantNumber,
          data: jsPsych.data.get().values()
        });

        return false;
      },
      data: { questionId: "consentForm" }
    });
  };

  // Consent
  addConsent();
  // Intro and practice
  addIntroAndPracticeTrials();

  // 'Real trials begin here' notice
  addNoticeHTML(
    "<p>The experiment is about to begin.</p>",
    "experimentAboutToBegin"
  );
  addBlockToTimeline(1);
  addBreak();
  addBlockToTimeline(2);

  // Factorial
  addNoticeHTML(
    "<p>We will begin a different task. Take a short break if you'd like, and click continue to proceed to the next set.</p>",
    "switchToFactorial"
  );
  addFactorial(timeline, state);

  // Back to tsp
  addNoticeHTML(
    "<p>We are now returning to the first task. Click continue to proceed.</p>",
    "returnTotsp"
  );
  addBlockToTimeline(3);
  addBreak();
  addBlockToTimeline(4);

  // Demographics

  addNoticeHTML(
    "<p>The trials have been completed. Please complete the following demographic form.</p>",
    "doneWithTrials"
  );

  timeline.push({
    type: "survey-html-form",
    html: normalText(DEMOGRAPHICS),
    data: { questionId: "demographics" }
  });

  // Debriefing statement
  timeline.push({
    type: "instructions",
    pages: [normalText(DEBRIEFING)],
    show_clickable_nav: true,
    data: { questionId: "debriefing" }
  });

  jsPsych.init({
    timeline: timeline,
    on_trial_finish: async data => {
      const trial_index = data.trial_index;
      serverlog(`Participant has completed trial_index: ${trial_index}`)
    },
    on_finish: onExperimentEnd
  });
}

// for debugging and development
window.tools = {
  endExperiment: onExperimentEnd
}

window.addEventListener("load", () => {
  document.body.innerHTML = `<h1>Loading...</h1>`;
  main();
});
