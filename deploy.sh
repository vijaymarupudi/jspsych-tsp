#!/usr/bin/env bash

npx webpack -p
ssh server << EOF
  mkdir /websites/experiments.vijaymarupudi.com/tsp -p
  mkdir /websites/experiments.vijaymarupudi.com/api -p
EOF
rsync dist/ server:/websites/experiments.vijaymarupudi.com/tsp -aP

rsync ../experiments-server/ server:/websites/experiments.vijaymarupudi.com/api/ -aP

ssh server << EOF
  sudo systemctl restart experiments-flask-server.service
EOF
