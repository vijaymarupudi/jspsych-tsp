#!/bin/bash

trap 'kill $(jobs -p) && exit' INT # ^C handler, SIGTERMs all jobs and exits

# running these in bg using job control
npx webpack-dev-server &
cd ../experiments-server
FLASK_ENV=development flask run &

# don't exit the script until jobs are done
wait
